This repository concludes two main projects on the following.

1. BackEnd
2. FrontEnd Website

**Install**<br />
After you clone this repository, open the folder and run the following command.<br />

inside back-end folder<br />
`npm install`<br />

inside front-end folder<br />
`yarn install`<br />

**Start BackEnd service**<br />
`cd back-end && npm start`<br />

**Start FrontEnd service**<br />
`cd front-end && yarn start`<br />


