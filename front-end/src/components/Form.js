import { Form, InputNumber, Button, Col, Row } from "antd";
import axios from "axios";
import { useState } from "react";

const MY_ENDPOINT = "http://localhost:8000/solution";

const divStyle = {
  backgroundColor: "#ffffff",
  padding: "20px",
  borderRadius: "5px",
};

const MyForm = ({ solution }) => {
  const [solutionResult, setSolution1Result] = useState();

  const onFinish = async ({ number }) => {
    if (number) {
    }

    const url = `${MY_ENDPOINT}${solution}/${number}`;
    const result = await axios({
      url,
      method: "get",
    });
    setSolution1Result(result.data);
  };

  const minMaxPair = {
    1: {
      min: 1,
      max: 10,
    },
    2: {
      min: 1,
      max: 25,
    },
    3: {
      min: 1,
      max: 20,
    },
  };

  return (
    <div style={{ ...divStyle }}>
      <Form name="basic" onFinish={onFinish}>
        <Form.Item>
          <Row justify="center">
            <Col style={{ marginRight: "20px" }}>
              <Form.Item label="Type your prefer N number" name="number">
                <InputNumber
                  min={minMaxPair[solution]?.min}
                  max={minMaxPair[solution]?.max}
                />
              </Form.Item>
            </Col>
            <Col>
              <Button
                type="dash"
                htmlType="submit"
                style={{ backgroundColor: "#025955", color: "#fde8cd" }}
              >
                Get result
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>

      {solutionResult &&
        solutionResult.result?.map((row) => {
          return (
            <Row align="middle" justify="center">
              {row.map((col) => {
                return (
                  <Col
                    style={{
                      width: "20px",
                      height: "20px",
                      backgroundColor: col === "X" ? "#75cfb8" : "#ffc478",
                      color: col === "X" ? "#f0e5d8" : "#f0e5d8",
                      alignSelf: "center",
                      margin: "3px",
                      borderRadius: "3px",
                      justifyContent: "center",
                      alignItems: "center",
                      display: "flex",
                    }}
                  >
                    {col}
                  </Col>
                );
              })}
            </Row>
          );
        })}
    </div>
  );
};

export default MyForm;
