import "./App.css";
import "./index.css";
import MyForm from "../src/components/Form";
import { Button, Radio } from "antd";
import { useState } from "react";

const divStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "100vh",
  backgroundColor: "#f0e5d8",
};

const mainDivStyle = {
  backgroundColor: "#f2f2f2",
  padding: "20px",
  borderRadius: "5px",
  boxShadow: "0px 6px 49px -18px rgba(105,105,105,1)",
  backdropFilter: "blur(10px)",
};

function App() {
  const [myPage, setMyPage] = useState(1);

  return (
    <div style={{ ...divStyle }}>
      <div style={{ ...mainDivStyle }}>
        <Radio.Group value={myPage} onChange={(e) => setMyPage(e.target.value)}>
          <Radio.Button value="1">Solution 1</Radio.Button>
          <Radio.Button value="2">Solution 2</Radio.Button>
          <Radio.Button value="3">Solution 3</Radio.Button>
        </Radio.Group>
        <MyForm solution={myPage} />
      </div>
    </div>
  );
}

export default App;
