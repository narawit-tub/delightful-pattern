function getResultFromSolutionTwo(n) {
  // frist fow and last row have x equal to N
  const CHAR1 = "";
  const CHAR2 = "X";

  let result = [];
  let resultStr = "";
  let acc = 1;

  for (let i = 1; i <= n; i++) {
    let row = [];

    for (let j = 1; j <= n; j++) {
      let char = "";
      if (j <= acc || j >= n - acc + 1) {
        // print X
        char = CHAR2;
      } else {
        // print O
        char = CHAR1;
      }

      row.push(char);
      resultStr = resultStr.concat(char);
    }

    if (n % 2 == 0) {
      if (i == Math.floor(n / 2)) {
        // do nothing
      } else if (i < Math.floor(n / 2) + 1) {
        acc++;
      } else {
        acc--;
      }
    } else {
      if (i < Math.floor(n / 2) + 1) {
        acc++;
      } else {
        acc--;
      }
    }

    result.push(row);
    resultStr = resultStr.concat("\n");
  }

  return result;
}

module.exports.getResultFromSolutionTwo = getResultFromSolutionTwo;
