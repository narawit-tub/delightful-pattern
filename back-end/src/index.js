const express = require("express");
const { getResultFromSolutionOne } = require("./solution1");
const { getResultFromSolutionTwo } = require("./solution2");
const { getResultFromSolutionThree } = require("./solution3");

const app = express();

app.get("/solution1/:number", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");

  const number = req.params.number;
  const result = getResultFromSolutionOne(number);

  res.send({ result });
});

app.get("/solution2/:number", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");

  const number = req.params.number;
  const result = getResultFromSolutionTwo(number);

  res.send({ result });
});

app.get("/solution3/:number", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");

  const number = req.params.number;
  const result = getResultFromSolutionThree(number);

  res.send({ result });
});

app.listen(8000, () => {
  console.log("Server is up on port 8000");
});
