function create2DArrayChar(CHAR2, size) {
  let resultMatrix = new Array(size + 1);

  for (let i = 1; i <= size; i++) {
    resultMatrix[i] = [];
    for (let j = 1; j <= size; j++) {
      resultMatrix[i][j] = CHAR2;
    }
  }
  return resultMatrix;
}

function getResultFromSolutionThree(n) {
  const CHAR1 = "Y";
  const CHAR2 = "X";

  let twoDArray = create2DArrayChar(CHAR2, n);

  if (n > 2) {
    let topBorder = 4;
    let rightBorder = n - 1;
    let leftBorder = 2;
    let bottomBorder = n - 1;
    let end = false;

    // traverse to element in spiral way
    let i = 2;
    let j = 1;

    twoDArray[i][j] = CHAR1;
    while (!end) {
      // go right
      if (j + 1 > rightBorder) end = true;
      while (j + 1 <= rightBorder && i <= bottomBorder) {
        j++;
        twoDArray[i][j] = CHAR1;
      }

      // go down
      if (i + 1 > bottomBorder) end = true;
      while (i + 1 <= bottomBorder) {
        i++;
        twoDArray[i][j] = CHAR1;
      }

      // go left
      if (j - 1 < leftBorder || i < topBorder) end = true;
      while (j - 1 >= leftBorder && i >= topBorder) {
        j--;
        twoDArray[i][j] = CHAR1;
      }

      // go up
      if (i - 1 < topBorder) end = true;
      while (i - 1 >= topBorder) {
        i--;
        twoDArray[i][j] = CHAR1;
      }

      topBorder = topBorder + 2;
      leftBorder = leftBorder + 2;
      rightBorder = rightBorder - 2;
      bottomBorder = bottomBorder - 2;
    }
  }

  // compact str for sending to front-end
  let result = [];

  for (let i = 1; i <= n; i++) {
    let row = [];
    for (let j = 1; j <= n; j++) {
      row.push(twoDArray[i][j]);
    }
    result.push(row);
  }
  return result;
}

module.exports.getResultFromSolutionThree = getResultFromSolutionThree;
