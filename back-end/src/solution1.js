function getResultFromSolutionOne(n) {
  // frist fow and last row have x equal to N
  const CHAR1 = "O";
  const CHAR2 = "X";

  let result = [];
  let resultStr = "";
  let numberOfOAtBeginAndLast = 0;

  for (let i = 1; i <= 2 * n - 1; i++) {
    let row = [];

    for (let j = 1; j <= 2 * n - 1; j++) {
      let char = "";

      if (j == 1) {
        if (numberOfOAtBeginAndLast == 0) {
          char = CHAR2;
        } else {
          char = CHAR1;
        }
      } else {
        let isOMode;
        let toggledPreviousChar = row[row.length - 1] == CHAR1 ? CHAR2 : CHAR1;

        if (
          j <= numberOfOAtBeginAndLast ||
          j >= 2 * n - 1 - (numberOfOAtBeginAndLast - 1)
        ) {
          isOMode = true;
        } else isOMode = false;

        // select Char
        if (isOMode) {
          char = CHAR1;
        } else {
          char = toggledPreviousChar;
        }
      }
      row.push(char);
      resultStr = resultStr.concat(char);
    }
    // change O increase/decrease
    if (i < n) {
      numberOfOAtBeginAndLast = numberOfOAtBeginAndLast + 1;
    } else numberOfOAtBeginAndLast = numberOfOAtBeginAndLast - 1;

    result.push(row);
    resultStr = resultStr.concat("\n");
  }

  return result;
}

module.exports.getResultFromSolutionOne = getResultFromSolutionOne;
